class CreateRedirectRules < ActiveRecord::Migration[5.1]
  def change
    create_table :redirect_rules do |t|
      t.string :source, null: false
      t.boolean :regex, null: false, default: false
      t.string :destination, null: false

      t.timestamps
    end
    add_index :redirect_rules, :source
  end
end
