RedirectRule.create!([
  {source: "http://written.rwdev.io/169188/swift-algorithm-club-august-2017-digest", regex: false, destination: "http://ios.rwdev.io/articles/169188"},
  {source: "http:\\/\\/videos.rwdev.io\\/courses\\/53-beginning-ios-10-part-1-getting-started\\/lessons\\/[0-9]*$", regex: true, destination: "http://ios.rwdev.io/courses/53-beginning-ios-10-part-1-getting-started/{match}"},
  {source: "http:\\/\\/written.rwdev.io\\/[0-9]*$", regex: true, destination: "http://ios.rwdev.io/articles/{match}"},
  {source: "http://localhost:3000/[0-9]*$", regex: true, destination: "http://ios.rwdev.io/articles/{match}"},
  {source: "http:\\/\\/written.rwdev.io\\/\\?p=[0-9]*.*", regex: true, destination: "http://ios.rwdev.io/articles/{match}"},
  {source: "http:\\/\\/written.rwdev.io\\/category\\/[a-z]*$", regex: true, destination: "http://{match}.rwdev.io/"},
  {source: "http:\\/\\/videos.rwdev.io\\/[a-z]*$", regex: true, destination: "http://ios.rwdev.io/library?filter={match}"}
])
