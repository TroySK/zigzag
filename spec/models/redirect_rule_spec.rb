require "rails_helper"

RSpec.describe RedirectRule, :type => :model do
  before :each do
    RedirectRule.delete_all
  end
  describe '#redirect' do
    context 'when there is no match' do
      it 'returns nil' do
        source_url = 'http://written.rwdev.io/169188'
        expect(RedirectRule.redirect(source_url)).to be_nil
      end
    end
    context 'when there is a match' do
      it 'returns nil' do
        RedirectRule.create({source: 'http:\\/\\/written.rwdev.io\\/[0-9]*$', regex: true, destination: 'http://ios.rwdev.io/articles/{match}'})
        source_url = 'http://written.rwdev.io/169188'
        expect(RedirectRule.redirect(source_url)).to eq('http://ios.rwdev.io/articles/169188')
      end
    end
  end
end
