# README

The app has been built and tested on Ruby 2.4.1. Other versions might now work.

To start please do a;
```
bundle install
```
Followed by;
```
rails db:create
```
and
```
rails db:migrate
```
- The Index page has some of the usages listed.
- The New Page has guidelines for creating new entries.

#### You may also use rails db:seed to load some seed data.

### There are 3 ways to use it;

- Redirection:
```
http://localhost:3000/transfer?source=[Your source URL]
```
The request will be redirected.
- API:
```
http://localhost:3000/api?source=[Your source URL]
```
The request will render a JSON response in the format ``` { "destination": "The Destination URL" } ```.
- Relative Redirection:
```
http://localhost:3000/[Your relative source URL]
```
The request will be redirected if entry is present for the current host.

In case of any doubts, please ping me on Skype; shirsendu.karmakar
