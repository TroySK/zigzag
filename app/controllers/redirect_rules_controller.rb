class RedirectRulesController < ApplicationController
  before_action :set_redirect_rule, only: [:show, :edit, :update, :destroy]

  # GET /redirect_rules
  # GET /redirect_rules.json
  def index
    @redirect_rules = RedirectRule.all
  end

  # GET /redirect_rules/1
  # GET /redirect_rules/1.json
  def show
  end

  # GET /redirect_rules/new
  def new
    @redirect_rule = RedirectRule.new
  end

  # GET /redirect_rules/1/edit
  def edit
  end

  # POST /redirect_rules
  # POST /redirect_rules.json
  def create
    @redirect_rule = RedirectRule.new(redirect_rule_params)

    respond_to do |format|
      if @redirect_rule.save
        format.html { redirect_to @redirect_rule, notice: 'Redirect rule was successfully created.' }
        format.json { render :show, status: :created, location: @redirect_rule }
      else
        format.html { render :new }
        format.json { render json: @redirect_rule.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /redirect_rules/1
  # PATCH/PUT /redirect_rules/1.json
  def update
    respond_to do |format|
      if @redirect_rule.update(redirect_rule_params)
        format.html { redirect_to @redirect_rule, notice: 'Redirect rule was successfully updated.' }
        format.json { render :show, status: :ok, location: @redirect_rule }
      else
        format.html { render :edit }
        format.json { render json: @redirect_rule.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /redirect_rules/1
  # DELETE /redirect_rules/1.json
  def destroy
    @redirect_rule.destroy
    respond_to do |format|
      format.html { redirect_to redirect_rules_url, notice: 'Redirect rule was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_redirect_rule
      @redirect_rule = RedirectRule.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def redirect_rule_params
      params.require(:redirect_rule).permit(:source, :regex, :destination)
    end
end
