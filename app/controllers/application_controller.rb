class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  def transfer
    source_url, request_type = set_source_and_type
    redirect_url = RedirectRule.redirect(source_url)
    if redirect_url.present?
      request_type == 'transfer' ? (redirect_to redirect_url) : (render json: { destination: redirect_url })
    else
      request_type == 'transfer' ? (render plain: '404 Not found', status: 404) : (render json: { error: 'not_found' }, status: 404)
    end
  end

  private

  def set_source_and_type
    if request.fullpath.match(/\A\/(transfer|api)\?source=/) # When source has payload
      source_url = request.fullpath.gsub(/\A\/(transfer|api)\?source=/, '')
      request_type = request.fullpath.match(/\A\/transfer\?source=/) ? 'transfer' : 'api'
    else # Local Request
      source_url = request.original_url
      request_type = 'transfer'
    end
    [source_url, request_type]
  end

end
