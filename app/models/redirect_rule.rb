class RedirectRule < ApplicationRecord
  validates_presence_of :source, message: 'Please provide a valid source.'
  validates_presence_of :destination, message: 'Please provide a destination.'

  # Returns the destination for a given source URL
  #
  # @param source_url [String] The source URL
  # @return [String] The destination URL if a match is found
  # @return [nil] When no match is found
  def self.redirect(source_url)
    conditions = "(redirect_rules.regex = false AND LOWER(redirect_rules.source) = LOWER('#{source_url}')) OR (redirect_rules.regex = true AND '#{source_url}' ~* redirect_rules.source)"
    redirect_rule = self.where(conditions).first
    if redirect_rule.present?
      if redirect_rule.regex?
        match_string = source_url.gsub(/#{redirect_rule.source.split('[')[0]}/, '')
        redirect_rule.destination.gsub('{match}', match_string)
      else
        redirect_rule.destination
      end
    end
  end
end
