Rails.application.routes.draw do
  resources :redirect_rules
  root to: 'redirect_rules#index'
  get '*path', to: 'application#transfer', format: false
end
